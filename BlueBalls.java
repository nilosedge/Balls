import java.applet.Applet; 
import java.awt.*; 
import java.awt.event.*; 

public class BlueBalls extends Applet implements Runnable, MouseListener { 
   private Thread blueBall; 
   private boolean xUp, yUp; 
   private int x, y, xDx, yDy; 
   private Thread greenBall; 
   private boolean gxUp, gyUp; 
   private int gx, gy, gxDx, gyDy; 
   private Thread redBall; 
   private boolean rxUp, ryUp; 
   private int rx, ry, rxDx, ryDy; 
   private Thread yellowBall; 
   private boolean wxUp, wyUp; 
   private int wx, wy, wxDx, wyDy; 

   public void init() { 
      xUp = false; 
      yUp = false; 
      xDx = 1; 
      yDy = 1; 
      gxUp = true; 
      gyUp = true; 
      gxDx = 1; 
      gyDy = 1; 
      gx = 290; 
      gy = 290; 
      rxUp = true; 
      ryUp = false; 
      rxDx = 1; 
      ryDy = 1; 
      rx = 290; 
      ry = 0; 
      wxUp = false; 
      wyUp = true; 
      wxDx = 1; 
      wyDy = 1; 
      wx = 0; 
      wy = 290; 
      addMouseListener( this ); 
   } 

   public void mousePressed( MouseEvent e ) { 
      if ( blueBall == null ) { 
         x = e.getX(); 
         y = e.getY(); 
         blueBall = new Thread( this ); 
         gx = e.getX(); 
         gy = e.getY(); 
         greenBall = new Thread( this ); 
         rx = e.getX(); 
         ry = e.getY(); 
         redBall = new Thread( this ); 
         wx = e.getX(); 
         wy = e.getY(); 
         yellowBall = new Thread( this ); 
         blueBall.start(); 
         greenBall.start(); 
         redBall.start(); 
         yellowBall.start(); 
       } 
   } 

   public void stop() { 
      if ( blueBall != null ) { 
         blueBall.stop(); 
         blueBall = null; 
         greenBall.stop(); 
         greenBall = null; 
         redBall.stop(); 
         redBall = null; 
         yellowBall.stop(); 
         yellowBall = null; 
      } 
   } 

   public void paint( Graphics g ) { 
      g.setColor( Color.blue ); 
      g.fillOval( x, y, 10, 10 ); 
      g.setColor( Color.green ); 
      g.fillOval( gx, gy, 10, 10 ); 
      g.setColor( Color.red ); 
      g.fillOval( rx, ry, 10, 10 ); 
      g.setColor( Color.yellow ); 
      g.fillOval( wx, wy, 10, 10 ); 
   } 

   public void run() { 
      while ( true ) { 
         try { 
            blueBall.sleep( 10 ); 
         } catch ( Exception e ) { 
            System.err.println( "Exception: " + e.toString() ); 
         } 

         if ( xUp == true ) 
            x += xDx; 
         else 
            x -= xDx; 

         if ( yUp == true ) 
            y += yDy; 
         else 
            y -= yDy; 

         if ( y <= 0 ) { 
            yUp = true; 
            yDy = ( int ) ( Math.random() * 5 + 2 ); 
         } 
         else if ( y >= 300) { 
            yDy = ( int ) ( Math.random() * 5 + 2 ); 
            yUp = false; 
         } 

         if ( x <= 0 ) { 
            xUp = true; 
            xDx = ( int ) ( Math.random() * 5 + 2 ); 
         } 
         else if ( x >= 300 ) { 
            xUp = false; 
            xDx = ( int ) ( Math.random() * 5 + 2 ); 
         } 

         try { 
            greenBall.sleep( 10 ); 
         } catch ( Exception e ) { 
            System.err.println( "Exception: " + e.toString() ); 
         } 

         if ( gxUp == true ) 
            gx += gxDx; 
         else 
            gx -= gxDx; 

         if ( gyUp == true ) 
            gy += gyDy; 
         else 
            gy -= gyDy; 

         if ( gy <= 0 ) { 
            gyUp = true; 
            gyDy = ( int ) ( Math.random() * 5 + 2 ); 
         } 
         else if ( gy >= 300) { 
            gyDy = ( int ) ( Math.random() * 5 + 2 ); 
            gyUp = false; 
         } 

         if ( gx <= 0 ) { 
            gxUp = true; 
            gxDx = ( int ) ( Math.random() * 5 + 2 ); 
         } 
         else if ( gx >= 300 ) { 
            gxUp = false; 
            gxDx = ( int ) ( Math.random() * 5 + 2 ); 
         } 

         try { 
            redBall.sleep( 10 ); 
          } catch ( Exception e ) { 
            System.err.println( "Exception: " + e.toString() ); 
         } 

         if ( rxUp == true ) 
            rx += rxDx; 
         else 
            rx -= rxDx; 

         if ( ryUp == true ) 
            ry += ryDy; 
        else 
            ry -= ryDy; 

         if ( ry <= 0 ) { 
            ryUp = true; 
            ryDy = ( int ) ( Math.random() * 5 + 2 ); 
         } 
         else if ( ry >= 300) { 
            ryDy = ( int ) ( Math.random() * 5 + 2 ); 
            ryUp = false; 
         } 

         if ( rx <= 0 ) { 
            rxUp = true; 
            rxDx = ( int ) ( Math.random() * 5 + 2 ); 
         } 
         else if ( rx >= 300 ) { 
            rxUp = false; 
            rxDx = ( int ) ( Math.random() * 5 + 2 ); 
         } 

         try { 
            yellowBall.sleep( 10 ); 
         } catch ( Exception e ) { 
            System.err.println( "Exception: " + e.toString() ); 
         } 

         if ( wxUp == true ) 
            wx += wxDx; 
         else 
            wx -= wxDx; 

         if ( wyUp == true ) 
            wy += wyDy; 
         else 
            wy -= wyDy; 

         if ( wy <= 0 ) { 
            wyUp = true; 
            wyDy = ( int ) ( Math.random() * 5 + 2 ); 
         } 
         else if ( wy >= 300) { 
            wyDy = ( int ) ( Math.random() * 5 + 2 ); 
            wyUp = false; 
         } 

         if ( wx <= 0 ) { 
            wxUp = true; 
            wxDx = ( int ) ( Math.random() * 5 + 2 ); 
         } 
         else if ( wx >= 300 ) { 
            wxUp = false; 
            wxDx = ( int ) ( Math.random() * 5 + 2 ); 
         } 

         repaint(); 
      } 
   } 

   public void mouseExited( MouseEvent e ) {} 
   public void mouseClicked( MouseEvent e ) {} 
   public void mouseReleased( MouseEvent e ) {} 
   public void mouseEntered( MouseEvent e ) {} 
} 
